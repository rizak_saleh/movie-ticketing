package com.nostratech.movieticketing.service;

import com.nostratech.movieticketing.dto.CityCreateDTO;

public interface CityService {
	
	public void createNewCity (CityCreateDTO dto);

}
