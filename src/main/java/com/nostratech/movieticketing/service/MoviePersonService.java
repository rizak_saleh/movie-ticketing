package com.nostratech.movieticketing.service;

import com.nostratech.movieticketing.dto.MoviePersonResponseDTO;

public interface MoviePersonService {

	public MoviePersonResponseDTO findMoviePersonDetail (Long moviePersonId);
	
	
}
