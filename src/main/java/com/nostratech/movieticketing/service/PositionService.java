package com.nostratech.movieticketing.service;

import java.util.List;


import com.nostratech.movieticketing.dto.PositionCreateDTO;
import com.nostratech.movieticketing.dto.PositionDetailDTO;
import com.nostratech.movieticketing.dto.PositionListResponseDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;

public interface PositionService {

	public PositionDetailDTO findPositionDetailById(Long positionId);

	public List<PositionDetailDTO> findPositionListDetail();

	public void createNewPosition(PositionCreateDTO dto);
	
	public ResultPageResponseDTO<PositionListResponseDTO> findPositionList(Integer pages, Integer limit, String sortBy, String direction, String name);
}
