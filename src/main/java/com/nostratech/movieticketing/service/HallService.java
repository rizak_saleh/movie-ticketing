package com.nostratech.movieticketing.service;

import com.nostratech.movieticketing.dto.HallCreateDTO;
import com.nostratech.movieticketing.dto.HallResponseDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;
import com.nostratech.movieticketing.dto.query.HallQueryDTO;

public interface HallService {
	
	public void createNewHall(Long theater_id, HallCreateDTO dto);
	
	public ResultPageResponseDTO<HallResponseDTO> findHallList (Integer pages, Integer limit, String sortBy, String direction, Long theaterId);

}
