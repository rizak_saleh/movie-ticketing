package com.nostratech.movieticketing.service;

import java.util.List;

import com.nostratech.movieticketing.domain.Movie;
import com.nostratech.movieticketing.dto.MovieCreateDTO;
import com.nostratech.movieticketing.dto.MovieDetailDTO;
import com.nostratech.movieticketing.dto.MovieDetailResponseDTO;
import com.nostratech.movieticketing.dto.MovieListResponseDTO;
import com.nostratech.movieticketing.dto.MoviePersonResponseDTO;
import com.nostratech.movieticketing.dto.MovieUpdateRequestDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;

public interface MovieService {
	
	public MovieDetailDTO findMovieDetailById(Long movieId);
	
	public List<MovieDetailDTO> findMovieListDetail();
	
	public void createNewMovie(List<MovieCreateDTO> dto);

	void createNewMovie(MovieCreateDTO dto);
	
	public MovieDetailResponseDTO findMovieDetail(Long movieId);
	
	public void updateMovie (Long movieId, MovieUpdateRequestDTO dto);
	
	public void deleteMovie (Long movieId);
	
	public List<MovieDetailResponseDTO> construcDTO(List<Movie> movies);
	
	public ResultPageResponseDTO<MovieListResponseDTO> findMovieList(Integer pages,
			Integer limit, String sortBy, String direction);

}
