package com.nostratech.movieticketing.service;

import com.nostratech.movieticketing.domain.City;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;
import com.nostratech.movieticketing.dto.TheaterCreateDTO;
import com.nostratech.movieticketing.dto.TheaterResponPageDTO;

public interface TheaterService {
	
	public void createNewTheater(TheaterCreateDTO dto);
	
	public ResultPageResponseDTO<TheaterResponPageDTO> findPersonList(Integer pages, Integer limit, String sortBy, String direction, String name);


}
