package com.nostratech.movieticketing.service;

import java.util.List;

import com.nostratech.movieticketing.dto.PersonCreateDTO;
import com.nostratech.movieticketing.dto.PersonDetailDTO;
import com.nostratech.movieticketing.dto.PersonListResponDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;

public interface PersonService {

	public PersonDetailDTO findPersonDetailById(Long personId);

	public List<PersonDetailDTO> findPersonListDetail();

	public void createNewPerson(List<PersonCreateDTO> dto);
	
	void createNewPerson (PersonCreateDTO dto);
	
	public ResultPageResponseDTO<PersonListResponDTO> findPersonList(Integer pages, Integer limit, String sortBy, String direction, String firstName);
}
