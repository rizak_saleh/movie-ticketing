package com.nostratech.movieticketing.service;

import java.util.List;

import com.nostratech.movieticketing.dto.CategoryCreateDTO;
import com.nostratech.movieticketing.dto.CategoryDetailDTO;
import com.nostratech.movieticketing.dto.CategoryListResponseDTO;
import com.nostratech.movieticketing.dto.CategoryResponseDTO;
import com.nostratech.movieticketing.dto.MovieDetailResponseDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;

public interface CategoryService {

	public CategoryDetailDTO findCategoryDetailById(Long categoryId);

	public List<CategoryDetailDTO> findCategoryListDetail();

	public void createNewCategory(CategoryCreateDTO dto);

	public CategoryResponseDTO findCategoryDetail(Long categoryId);

	public ResultPageResponseDTO<CategoryListResponseDTO> findCategoryList(Integer pages, Integer limit, String sortBy,
			String direction);
}
