package com.nostratech.movieticketing.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.nostratech.movieticketing.domain.Movie;
import com.nostratech.movieticketing.domain.MoviePerson;
import com.nostratech.movieticketing.dto.MovieDTO;
import com.nostratech.movieticketing.dto.MoviePersonResponseDTO;
import com.nostratech.movieticketing.dto.PersonDTO;
import com.nostratech.movieticketing.dto.PositionDTO;
import com.nostratech.movieticketing.exception.BadRequestException;
import com.nostratech.movieticketing.repository.MoviePersonRepository;
import com.nostratech.movieticketing.repository.MovieRepository;
import com.nostratech.movieticketing.service.MoviePersonService;
import com.nostratech.movieticketing.service.MovieService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class MoviePersonServiceImpl implements MoviePersonService {

	private final MovieRepository movieRepository;

	private final MoviePersonRepository moviePersonRepository;
	
	private final MovieService movieService;

	@Override
	public MoviePersonResponseDTO findMoviePersonDetail(Long moviePersonId) {
		MoviePerson moviePerson = moviePersonRepository.findById(moviePersonId)
				.orElseThrow(() -> new BadRequestException("invalid.movie.person.id"));
		MoviePersonResponseDTO dto = new MoviePersonResponseDTO();
		
		List<Movie> movie = new ArrayList<>();
		MovieDTO movieDTO = new MovieDTO();
		
		movieDTO.setId(moviePerson.getMovie().getId());
		movieDTO.setTitle(moviePerson.getMovie().getTitle());
		dto.setMovie(movieDTO);
		
		PersonDTO personDTO = new PersonDTO();
		personDTO.setFirstName(moviePerson.getPerson().getFirstName());
		personDTO.setLastName(moviePerson.getPerson().getLastName());
		dto.setFirstName(personDTO.getFirstName());
		dto.setLastName(personDTO.getLastName());
		
		PositionDTO positionDTO = new PositionDTO();
		positionDTO.setPosition(moviePerson.getPosition().getName());
		dto.setPosition(positionDTO.getPosition());
		
		return dto;
		
		
		
//		List<Movie> movies = (List<Movie>) moviePerson.getMovie();
//		list<MovieDTO> movieDTOs = movies.stream().map(m-> {
//			MovieDTO movieDTO = new MovieDTO();
//			dto.setMovies(m.getId());
//			dto.setMovies(m.getTitle());
//			return dto;
//		}).collect(Collectors.toList());
//		return dto;
	}

}
