package com.nostratech.movieticketing.service.impl;

import org.springframework.stereotype.Service;

import com.nostratech.movieticketing.domain.City;
import com.nostratech.movieticketing.dto.CityCreateDTO;
import com.nostratech.movieticketing.repository.CityRepository;
import com.nostratech.movieticketing.service.CityService;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class CityServiceImpl implements CityService{
	
	private final CityRepository cityRepository;

	@Override
	public void createNewCity(CityCreateDTO dto) {
		City city = new City();
		city.setName(dto.getCityName());
		cityRepository.save(city);
		
	}

}
