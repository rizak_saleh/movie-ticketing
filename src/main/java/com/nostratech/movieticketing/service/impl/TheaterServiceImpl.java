package com.nostratech.movieticketing.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.nostratech.movieticketing.domain.City;
import com.nostratech.movieticketing.domain.Theater;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;
import com.nostratech.movieticketing.dto.TheaterCreateDTO;
import com.nostratech.movieticketing.dto.TheaterResponPageDTO;
import com.nostratech.movieticketing.exception.BadRequestException;
import com.nostratech.movieticketing.repository.CityRepository;
import com.nostratech.movieticketing.repository.TheaterRepository;
import com.nostratech.movieticketing.service.TheaterService;
import com.nostratech.movieticketing.util.PaginationUtil;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class TheaterServiceImpl implements TheaterService {

	private final CityRepository cityRepository;

	private final TheaterRepository theaterRepository;

	@Override
	public void createNewTheater(TheaterCreateDTO dto) {
		City city = cityRepository.findCityIdById(dto.getCityId()).orElseThrow(() -> new BadRequestException("invalid.city.id"));  
		
		Theater theater = new Theater();
		theater.setName(dto.getTheaterName());
		theater.setCity(city);
		theaterRepository.save(theater);

	}

	@Override
	public ResultPageResponseDTO<TheaterResponPageDTO> findPersonList(Integer pages, Integer limit, String sortBy,
			String direction, String cityName) {
		City city = new City();
		cityName = StringUtils.isEmpty(cityName)? "%" : "%" +  cityName + "%";
		Sort sort = Sort.by(new Sort.Order(PaginationUtil.getSortBy(direction), sortBy));
		Pageable Pageable = PageRequest.of(pages, limit, sort);
		Page<Theater> pageResult = theaterRepository.findTheaterList(cityName ,Pageable);
		List<TheaterResponPageDTO> dtos = pageResult.stream().map((t)->{
			TheaterResponPageDTO dto = new TheaterResponPageDTO();
			dto.setTheaterId(t.getId().toString());
			dto.setTheaterName(t.getName());
			return dto;
		}).collect(Collectors.toList());
		
		return PaginationUtil.createResultPageDTO(dtos, pageResult.getTotalElements() , pageResult.getTotalPages());
	}

}
