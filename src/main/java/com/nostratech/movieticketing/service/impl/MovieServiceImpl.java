package com.nostratech.movieticketing.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import com.nostratech.movieticketing.domain.Category;
import com.nostratech.movieticketing.domain.Movie;
import com.nostratech.movieticketing.domain.MoviePerson;
import com.nostratech.movieticketing.domain.Person;
import com.nostratech.movieticketing.domain.Position;
import com.nostratech.movieticketing.dto.CategoryDTO;
import com.nostratech.movieticketing.dto.CategoryResponseDTO;
import com.nostratech.movieticketing.dto.MovieCreateDTO;
import com.nostratech.movieticketing.dto.MovieDetailDTO;
import com.nostratech.movieticketing.dto.MovieDetailResponseDTO;
import com.nostratech.movieticketing.dto.MovieListResponseDTO;
import com.nostratech.movieticketing.dto.MoviePersonDTO;
import com.nostratech.movieticketing.dto.MoviePersonDetailDTO;
import com.nostratech.movieticketing.dto.MoviePersonResponseDTO;
import com.nostratech.movieticketing.dto.MovieUpdateRequestDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;
import com.nostratech.movieticketing.exception.BadRequestException;
import com.nostratech.movieticketing.repository.CategoryRepository;
import com.nostratech.movieticketing.repository.MoviePersonRepository;
import com.nostratech.movieticketing.repository.MovieRepository;
import com.nostratech.movieticketing.repository.PersonRepository;
import com.nostratech.movieticketing.repository.PositionRepository;
import com.nostratech.movieticketing.service.MovieService;
import com.nostratech.movieticketing.util.PaginationUtil;

import lombok.AllArgsConstructor;

@Service
public class MovieServiceImpl implements MovieService {

	private final MovieRepository movieRepository;

	private final CategoryRepository categoryRepository;

	private final MoviePersonRepository moviePersonRepository;

	private final PersonRepository personRepository;

	private final PositionRepository positionRepository;

	public MovieServiceImpl(MovieRepository movieRepository, CategoryRepository categoryRepository,
			MoviePersonRepository moviePersonRepository, PersonRepository personRepository,
			PositionRepository positionRepository) {
		super();
		this.movieRepository = movieRepository;
		this.categoryRepository = categoryRepository;
		this.moviePersonRepository = moviePersonRepository;
		this.personRepository = personRepository;
		this.positionRepository = positionRepository;
	}

	@Override
	public MovieDetailDTO findMovieDetailById(Long movieId) {
		Movie movie = (Movie) movieRepository.findAll(); // harus pakai optional
		MovieDetailDTO dto = new MovieDetailDTO();
		dto.setMovieId(movie.getId());

		dto.setMovieTitle(movie.getTitle());
		dto.setMovieDescription(movie.getDescription());
		return dto;
	}

	@Override
	public List<MovieDetailDTO> findMovieListDetail() {
		List<Movie> movies = movieRepository.findAll();

		return movies.stream().map((b) -> {
			MovieDetailDTO dto = new MovieDetailDTO();

			dto.setMovieDescription(b.getDescription());
			dto.setMovieId(b.getId());
			dto.setMovieTitle(b.getTitle());
//			dto.setCategoryIdList(b.getCategories().stream().map(c->c.getId()).toList());
//			dto.setMoviePersonIdList(

			return dto;
		}).collect(Collectors.toList());
	}

	@Transactional
	@Override
	public void createNewMovie(MovieCreateDTO dto) {

		List<Category> categories = categoryRepository.findByIdIn(dto.getCategoryIdList());

		Movie movie = new Movie();

		movie.setTitle(dto.getMovieTitle());
		movie.setDescription(dto.getDescription());
		movie.setCategories(categories);
		movie = movieRepository.save(movie);

		List<MoviePerson> moviePersons = new ArrayList<>();
		for (MoviePersonDetailDTO mp : dto.getMoviePersons()) {
			System.out.println(mp.getPersonId());
			Person person = personRepository.findById(mp.getPersonId())
					.orElseThrow(() -> new BadRequestException("invalid.person.id"));
			Position position = positionRepository.findById(mp.getPositionId())
					.orElseThrow(() -> new BadRequestException("invalid.position.id"));

			MoviePerson moviePerson = new MoviePerson();
			moviePerson.setMovie(movie);
			moviePerson.setPerson(person);
			moviePerson.setPosition(position);
			moviePersons.add(moviePerson);

		}
		moviePersonRepository.saveAll(moviePersons);

	}

	@Override
	public void createNewMovie(List<MovieCreateDTO> dto) {

	}

	@Override
	public MovieDetailResponseDTO findMovieDetail(Long movieId) {

		Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new BadRequestException("invalid.movie.id"));
		MovieDetailResponseDTO dto = new MovieDetailResponseDTO();
		dto.setId(movie.getId());
		dto.setTitle(movie.getTitle());
		dto.setDescription(movie.getDescription());

		List<Category> categories = movie.getCategories();
		List<CategoryDTO> categoryDTOs = categories.stream().map(c -> {
			CategoryDTO categoryDTO = new CategoryDTO();
			categoryDTO.setId(c.getId());
			categoryDTO.setName(c.getName());
			return categoryDTO;
		}).collect(Collectors.toList());

		dto.setCategories(categoryDTOs);

		List<MoviePerson> moviePersons = moviePersonRepository.findByMovieId(movieId);
		// List<MoviePerson> newmoviePersons = new ArrayList<MoviePerson>();
		// MoviePersonResponseDTO

//		
		List<MoviePersonDTO> mpDTOs = moviePersons.stream().map(mp -> {
			MoviePersonDTO mpDTO = new MoviePersonDTO();
			mpDTO.setPerson(mp.getPerson());
			mpDTO.setPosition(mp.getPosition());
			return mpDTO;
		}).collect(Collectors.toList());

		dto.setMoviePersons(mpDTOs);

		return dto;

	}

	// UPDATE
	@Override
	public void updateMovie(Long movieId, MovieUpdateRequestDTO dto) {
		Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new BadRequestException("invalid.movie.id"));
		movie.setTitle(dto.getMovieTitle() == null ? movie.getTitle() : dto.getMovieTitle());
		movie.setDescription(dto.getDescription() == null ? movie.getDescription() : dto.getDescription());

		movieRepository.save(movie);

	}

	// DELETE
	@Override
	public void deleteMovie(Long movieId) {
		movieRepository.deleteById(movieId);

	}

	@Override
	public List<MovieDetailResponseDTO> construcDTO(List<Movie> movies) {

		return movies.stream().map((m) -> {
			MovieDetailResponseDTO dto = new MovieDetailResponseDTO();
			dto.setId(m.getId());
			dto.setTitle(m.getTitle());
			return dto;
		}).collect(Collectors.toList());
	}

	@Override
	public ResultPageResponseDTO<MovieListResponseDTO> findMovieList(Integer pages, Integer limit, String sortBy,
			String direction) {
		// buat pageable
		// buat sort dulu
		Sort sort = Sort.by(new Sort.Order(PaginationUtil.getSortBy(direction), sortBy));
		Pageable pageable = PageRequest.of(pages, limit, sort);
		// query dalam bentuk pagination
		//Page<Movie> pageResult = movieRepository.findByTitleLikeIgnoreCase(movieTitle, pageable);
		Page<Movie> pageResult = movieRepository.findMovieList(pageable);
		// Parcing value entitiy ke payload
		List<MovieListResponseDTO> dtos = pageResult.stream().map((m) -> {
			MovieListResponseDTO dto = new MovieListResponseDTO();
			dto.setId(m.getId());
			dto.setTitle(m.getTitle());
			dto.setDescription(m.getDescription());

			return dto;
		}).collect(Collectors.toList());
		return PaginationUtil.createResultPageDTO(dtos, pageResult.getTotalElements(), pageResult.getTotalPages());
	}

}
