package com.nostratech.movieticketing.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.nostratech.movieticketing.domain.Person;
import com.nostratech.movieticketing.domain.Position;
import com.nostratech.movieticketing.dto.PersonCreateDTO;
import com.nostratech.movieticketing.dto.PersonDetailDTO;
import com.nostratech.movieticketing.dto.PersonListResponDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;
import com.nostratech.movieticketing.repository.PersonRepository;
import com.nostratech.movieticketing.repository.PositionRepository;
import com.nostratech.movieticketing.service.PersonService;
import com.nostratech.movieticketing.util.PaginationUtil;

import lombok.AllArgsConstructor;



@AllArgsConstructor
@Service("personService")
public class PersonServiceImpl implements PersonService {

	private final PersonRepository personRepository;
	

	@Override
	public PersonDetailDTO findPersonDetailById(Long personId) {
		Person person = (Person) personRepository.findAll();
		PersonDetailDTO dto = new PersonDetailDTO();
		dto.setPersonId(person.getId());

		dto.setPersonFirstName(person.getFirstName());
		dto.setPersonLastName(person.getLastName());
		return dto;
	}

	@Override
	public List<PersonDetailDTO> findPersonListDetail() {
		List<Person> persons = personRepository.findAll();
		
		return persons.stream().map((b) -> {
			PersonDetailDTO dto = new PersonDetailDTO();

			dto.setPersonFirstName(b.getFirstName());
			dto.setPersonId(b.getId());
			dto.setPersonLastName(b.getLastName());
			return dto;
		}).collect(Collectors.toList());
	}

	@Override
	public void createNewPerson(PersonCreateDTO dto) {

		Person person = new Person();

		person.setFirstName(dto.getPersonFirstName());
		person.setLastName(dto.getPersonLastName());
		personRepository.save(person);

	}

	@Override
	public void createNewPerson(List<PersonCreateDTO> dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ResultPageResponseDTO<PersonListResponDTO> findPersonList(Integer pages, Integer limit, String sortBy,
			String direction, String firstName) {
		firstName = StringUtils.isEmpty(firstName) ? "%" : "%" + firstName + "%";
		Sort sort = Sort.by(new Sort.Order(PaginationUtil.getSortBy(direction), sortBy));
		Pageable Pageable = PageRequest.of(pages, limit, sort);
		Page<Person> pageResult = personRepository.findPersonList(firstName, Pageable);
		List<PersonListResponDTO> dtos = pageResult.stream().map((p)->{
			PersonListResponDTO dto = new PersonListResponDTO();
			dto.setPersonId(p.getId());
			dto.setPersonFirstName(p.getFirstName());
			dto.setPersonLastName(p.getLastName());
			return dto;
		}).collect(Collectors.toList());
		return PaginationUtil.createResultPageDTO(dtos, pageResult.getTotalElements() , pageResult.getTotalPages());
	}



}
