package com.nostratech.movieticketing.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.nostratech.movieticketing.domain.Category;
import com.nostratech.movieticketing.domain.Position;
import com.nostratech.movieticketing.dto.CategoryListResponseDTO;
import com.nostratech.movieticketing.dto.PositionCreateDTO;
import com.nostratech.movieticketing.dto.PositionDetailDTO;
import com.nostratech.movieticketing.dto.PositionListResponseDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;
import com.nostratech.movieticketing.repository.PositionRepository;
import com.nostratech.movieticketing.service.PositionService;
import com.nostratech.movieticketing.util.PaginationUtil;

import lombok.AllArgsConstructor;



@AllArgsConstructor
@Service("positionService")
public class PositionServiceImpl implements PositionService {

	private final PositionRepository positionRepository;

	@Override
	public PositionDetailDTO findPositionDetailById(Long positionId) {
		Position position = (Position) positionRepository.findAll();
		PositionDetailDTO dto = new PositionDetailDTO();
		dto.setPositionId(position.getId());

		dto.setPositionName(position.getName());
		return dto;
	}

	@Override
	public List<PositionDetailDTO> findPositionListDetail() {
		List<Position> positions = positionRepository.findAll();
		return positions.stream().map((b) -> {
			PositionDetailDTO dto = new PositionDetailDTO();

			dto.setPositionName(b.getName());
			dto.setPositionId(b.getId());
			return dto;
		}).collect(Collectors.toList());
	}

	@Override
	public void createNewPosition(PositionCreateDTO dto) {

		Position position = new Position();

		position.setName(dto.getPositionName());
		positionRepository.save(position);

	}

	@Override
	public ResultPageResponseDTO<PositionListResponseDTO> findPositionList(Integer pages, Integer limit, String sortBy,
			String direction, String name) {
		
		name = StringUtils.isEmpty(name) ? "%" : name + "%";
		Sort sort = Sort.by(new Sort.Order(PaginationUtil.getSortBy(direction), sortBy));
		Pageable pageable = PageRequest.of(pages, limit, sort);
		Page<Position> pageResult = positionRepository.findPositionList(name, pageable);
		List<PositionListResponseDTO> dtos = pageResult.stream().map((p) -> {
			PositionListResponseDTO dto = new PositionListResponseDTO();
			dto.setPositionId(p.getId().toString());
			dto.setPositionName(p.getName());
			return dto;
		}).collect(Collectors.toList());
		return PaginationUtil.createResultPageDTO(dtos, pageResult.getTotalElements(), pageResult.getTotalPages());
	}



}
