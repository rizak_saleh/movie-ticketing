package com.nostratech.movieticketing.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.nostratech.movieticketing.domain.Category;
import com.nostratech.movieticketing.dto.CategoryCreateDTO;
import com.nostratech.movieticketing.dto.CategoryDetailDTO;
import com.nostratech.movieticketing.dto.CategoryListResponseDTO;
import com.nostratech.movieticketing.dto.CategoryResponseDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;
import com.nostratech.movieticketing.exception.BadRequestException;
import com.nostratech.movieticketing.repository.CategoryRepository;
import com.nostratech.movieticketing.service.CategoryService;
import com.nostratech.movieticketing.util.PaginationUtil;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

	private final CategoryRepository categoryRepository;

	@Override
	public CategoryDetailDTO findCategoryDetailById(Long categoryId) {
		Category category = (Category) categoryRepository.findAll();
		CategoryDetailDTO dto = new CategoryDetailDTO();
		dto.setCategoryId(category.getId());

		dto.setCategoryName(category.getName());
		return dto;
	}

	@Override
	public List<CategoryDetailDTO> findCategoryListDetail() {
		List<Category> categorys = categoryRepository.findAll();
		return categorys.stream().map((b) -> {
			CategoryDetailDTO dto = new CategoryDetailDTO();

			dto.setCategoryName(b.getName());
			dto.setCategoryId(b.getId());
			return dto;
		}).collect(Collectors.toList());
	}

	@Override
	public void createNewCategory(CategoryCreateDTO dto) {

		Category category = new Category();

		category.setName(dto.getCategoryName());
		categoryRepository.save(category);

	}

	@Override
	public CategoryResponseDTO findCategoryDetail(Long categoryId) {
		Category category = categoryRepository.findById(categoryId)
				.orElseThrow(() -> new BadRequestException("invalid.category.id"));
		CategoryResponseDTO dto = new CategoryResponseDTO();
		dto.setId(category.getId());
		dto.setName(category.getName());
		return dto;
	}

	@Override
	public ResultPageResponseDTO<CategoryListResponseDTO> findCategoryList(Integer pages, Integer limit, String sortBy,
			String direction) {
		//categoryName = StringUtils.isEmpty(categoryName) ? "%" : categoryName + "%";
		Sort sort = Sort.by(new Sort.Order(PaginationUtil.getSortBy(direction), sortBy));
		Pageable pageable = PageRequest.of(pages, limit, sort);
		Page<Category> pageResult = categoryRepository.findAll(pageable);
		List<CategoryListResponseDTO> dtos = pageResult.stream().map((p) -> {
			CategoryListResponseDTO dto = new CategoryListResponseDTO();
			dto.setCategoryId(p.getId().toString());
			dto.setCategoryName(p.getName());
			return dto;
		}).collect(Collectors.toList());
		return PaginationUtil.createResultPageDTO(dtos, pageResult.getTotalElements(), pageResult.getTotalPages());
	}

}
