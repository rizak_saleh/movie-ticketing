package com.nostratech.movieticketing.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.nostratech.movieticketing.domain.Hall;
import com.nostratech.movieticketing.domain.Theater;
import com.nostratech.movieticketing.dto.HallCreateDTO;
import com.nostratech.movieticketing.dto.HallResponseDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;
import com.nostratech.movieticketing.dto.query.HallQueryDTO;
import com.nostratech.movieticketing.exception.BadRequestException;
import com.nostratech.movieticketing.repository.HallRepository;
import com.nostratech.movieticketing.repository.TheaterRepository;
import com.nostratech.movieticketing.service.HallService;
import com.nostratech.movieticketing.util.PaginationUtil;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class HallServiceImpl implements HallService {

	private final TheaterRepository theaterRepository;

	private final HallRepository hallRepository;

	@Override
	public void createNewHall(Long theater_id, HallCreateDTO dto) {
		Theater theater = theaterRepository.findTheaterIdById(theater_id)
				.orElseThrow(() -> new BadRequestException("invalid.theater.id"));

		Hall hall = new Hall();
		hall.setName(dto.getHallName());
		hall.setTheater(theater);

		hallRepository.save(hall);

	}

	@Override
	public ResultPageResponseDTO<HallResponseDTO> findHallList(Integer pages, Integer limit, String sortBy,
			String direction, Long theaterId) {
//		Theater theater = theaterRepository.findTheaterIdById(theaterId).orElseThrow(()-> new BadRequestException("invalid.theater.id"));
		Sort sort = Sort.by(new Sort.Order(PaginationUtil.getSortBy(direction), sortBy));
		Pageable Pageable = PageRequest.of(pages, limit, sort);
		Page<HallQueryDTO> pageResult = hallRepository.findHallList(theaterId, Pageable);
		List<HallResponseDTO> dtos = pageResult.stream().map((h)->{
			HallResponseDTO dto = new HallResponseDTO();
			dto.setHallId(h.getHallId().toString());
			dto.setHallName(h.getHallName());
			dto.setTheaterName(h.getTheaterName());
			return dto;
		}).collect(Collectors.toList());
		
		return PaginationUtil.createResultPageDTO(dtos, pageResult.getTotalElements() , pageResult.getTotalPages());
	}

}
