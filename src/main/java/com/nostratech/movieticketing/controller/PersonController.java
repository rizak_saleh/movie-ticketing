package com.nostratech.movieticketing.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nostratech.movieticketing.service.PersonService;


import lombok.AllArgsConstructor;

import com.nostratech.movieticketing.dto.PersonCreateDTO;
import com.nostratech.movieticketing.dto.PersonDetailDTO;


@AllArgsConstructor
@Controller
@RequestMapping("/person")
public class PersonController {

	private final PersonService personService;

	@GetMapping("/list")
	public String findPersonList(Model model) {
		List<PersonDetailDTO> persons = personService.findPersonListDetail();
		model.addAttribute("persons", persons);
		return "movies/listPerson";
	}

	@GetMapping("/new")
	public String loadPersonForm(Model model) {
		PersonCreateDTO dto = new PersonCreateDTO();
		model.addAttribute("personCreateDTO", dto);
		return "movies/person-new";
	}

	@PostMapping("/new")
	public String addNewPerson(@ModelAttribute("personCreateDTO") @Valid PersonCreateDTO dto, BindingResult bindingResult,
			Errors errors, Model model) {

		if(errors.hasErrors()) {
			model.addAttribute("personCreateDTO", dto);
			return "movies/person-new";
		}
		personService.createNewPerson(dto);
		return "redirect:/person/list";

	}
}