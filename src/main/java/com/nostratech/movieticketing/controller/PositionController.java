package com.nostratech.movieticketing.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nostratech.movieticketing.service.PositionService;

import lombok.AllArgsConstructor;

import com.nostratech.movieticketing.dto.PositionCreateDTO;
import com.nostratech.movieticketing.dto.PositionDetailDTO;

@AllArgsConstructor
@Controller
@RequestMapping("/position")
public class PositionController {

	private final PositionService positionService;

	@GetMapping("/list")
	public String findPositionList(Model model) {
		List<PositionDetailDTO> positions = positionService.findPositionListDetail();
		model.addAttribute("positions", positions);
		return "movies/listPosition";
	}

	@GetMapping("/new")
	public String loadPositionForm(Model model) {
		PositionCreateDTO dto = new PositionCreateDTO();
		model.addAttribute("positionCreateDTO", dto);
		return "movies/position-new";
	}

	@PostMapping("/new")
	public String addNewPosition(@ModelAttribute("positionCreateDTO") @Valid PositionCreateDTO dto, BindingResult bindingResult,
			Errors errors, Model model) {

		if(errors.hasErrors()) {
			model.addAttribute("positionCreateDTO", dto);
			return "movies/position-new";
		}
		positionService.createNewPosition(dto);
		return "redirect:/position/list";

	}
}
