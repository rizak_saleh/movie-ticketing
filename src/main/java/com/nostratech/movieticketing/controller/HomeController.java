package com.nostratech.movieticketing.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller 
public class HomeController {

	@GetMapping("/home")
	public String getHome(Model model) {
		model.addAttribute("name","Rizak");
		return "home";
	}
}
