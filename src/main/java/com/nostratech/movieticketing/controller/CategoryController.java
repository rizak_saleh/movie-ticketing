package com.nostratech.movieticketing.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nostratech.movieticketing.service.CategoryService;

import lombok.AllArgsConstructor;

import com.nostratech.movieticketing.dto.CategoryCreateDTO;
import com.nostratech.movieticketing.dto.CategoryDetailDTO;

@AllArgsConstructor
@Controller
@RequestMapping("/category")
public class CategoryController {

	private final CategoryService categoryService;

	@GetMapping("/list")
	public String findCategoryList(Model model) {
		List<CategoryDetailDTO> categories = categoryService.findCategoryListDetail();
		model.addAttribute("categories", categories);
		return "movies/listCategory";
	}

	@GetMapping("/new")
	public String loadCategoryForm(Model model) {
		CategoryCreateDTO dto = new CategoryCreateDTO();
		model.addAttribute("categoryCreateDTO", dto);
		return "movies/category-new";
	}

	@PostMapping("/new")
	public String addNewCategory(@ModelAttribute("categoryCreateDTO") @Valid CategoryCreateDTO dto, BindingResult bindingResult,
			Errors errors, Model model) {

		if(errors.hasErrors()) {
			model.addAttribute("categoryCreateDTO", dto);
			return "movies/category-new";
		}
		categoryService.createNewCategory(dto);
		return "redirect:/category/list";

	}
}
