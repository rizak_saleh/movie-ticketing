package com.nostratech.movieticketing.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nostratech.movieticketing.service.MovieService;

import lombok.AllArgsConstructor;

import com.nostratech.movieticketing.dto.MovieCreateDTO;
import com.nostratech.movieticketing.dto.MovieDetailDTO;

@AllArgsConstructor
@Controller
@RequestMapping("/movie")
public class MovieController {

	private final MovieService movieService;

	@GetMapping("/list")
	public String findMovieList(Model model) {
		List<MovieDetailDTO> movies = movieService.findMovieListDetail();
		model.addAttribute("movies", movies);
		return "movies/list";
	}

	@GetMapping("/new")
	public String loadMovieForm(Model model) {
		MovieCreateDTO dto = new MovieCreateDTO();
		model.addAttribute("movieCreateDTO", dto);
		return "movies/movie-new";
	}

	@PostMapping("/new")
	public String addNewMovie(@ModelAttribute("movieCreateDTO") @Valid MovieCreateDTO dto, BindingResult bindingResult,
			Errors errors, Model model) {

		if(errors.hasErrors()) {
			model.addAttribute("movieCreateDTO", dto);
			return "movies/movie-new";
		}
		movieService.createNewMovie(dto);
		return "redirect:/movie/list";

	}
}
