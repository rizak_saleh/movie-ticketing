package com.nostratech.movieticketing.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller //special annotation -> Spring MVC(module)
public class HelloController {

	@GetMapping("/hello")
	public String getHello() {
		return "Hello";
	}
}
