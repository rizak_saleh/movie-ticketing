package com.nostratech.movieticketing.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nostratech.movieticketing.domain.Position;


public interface PositionRepository extends JpaRepository<Position, Long>{

	List<Position> findByIdIn(List<Long> idList);
	//SELECT DISTINCT COL_NAME FROM myTable WHERE UPPER(COL_NAME) LIKE UPPER('%PriceOrder%')
	@Query(value = "SELECT * FROM position where UPPER(name) like UPPER(:name)  ", nativeQuery = true)
	public Page<Position> findPositionList(String name, Pageable pageable);
}
