package com.nostratech.movieticketing.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nostratech.movieticketing.domain.Hall;
import com.nostratech.movieticketing.dto.query.HallQueryDTO;

public interface HallRepository extends JpaRepository<Hall, Long> {

	@Query("SELECT new com.nostratech.movieticketing.dto.query.HallQueryDTO(h.id, h.name, t.name) FROM Hall h "
			+ "INNER JOIN Theater t on t.id = h.theater.id "
			+ "WHERE (t.id = :theaterId)")
	public Page<HallQueryDTO> findHallList (Long theaterId, Pageable pageable);
}
