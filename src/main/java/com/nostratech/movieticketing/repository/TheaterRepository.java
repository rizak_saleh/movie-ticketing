package com.nostratech.movieticketing.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nostratech.movieticketing.domain.City;
import com.nostratech.movieticketing.domain.Theater;

public interface TheaterRepository extends JpaRepository<Theater, Long>{

	@Query(value = "select t.id, t.name, t.city_id, c.name from theater t inner join city c on c.id = t.city_id where UPPER(c.name) like UPPER(:cityName)", nativeQuery = true)
	public Page<Theater> findTheaterList (String cityName, Pageable pageable);
}
