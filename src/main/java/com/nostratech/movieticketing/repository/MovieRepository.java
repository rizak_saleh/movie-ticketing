package com.nostratech.movieticketing.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nostratech.movieticketing.domain.Category;
import com.nostratech.movieticketing.domain.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long>{
	
	public Page<Movie> findByTitleLikeIgnoreCase(String movieTitle, Pageable pageable);
	
	@Query("SELECT m from Movie m ")
	public Page<Movie> findMovieList(Pageable pageable);
	
	//public Page<Category> findAll(Pageable pageable)

}
