package com.nostratech.movieticketing.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nostratech.movieticketing.domain.Person;

public interface PersonRepository extends JpaRepository<Person, Long>{

	//List<Person> findByIdIn(List<MoviePerson> idList);

	@Query("SELECT p FROM Person p WHERE (UPPER(firstName) like UPPER(:firstName))")
	public Page<Person> findPersonList (String firstName, Pageable pageable);
	
}