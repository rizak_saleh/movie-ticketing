package com.nostratech.movieticketing.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nostratech.movieticketing.domain.Category;


public interface CategoryRepository extends JpaRepository<Category, Long>{

	List<Category> findByIdIn(List<Long> idList);
	
	//List<Category> findCategoryByMovieId(Long movieId);
	
	public Page<Category> findByNameLikeIgnoreCase(String categoryName, Pageable pageable);
	
	public Page<Category> findAll(Pageable pageable);
}
