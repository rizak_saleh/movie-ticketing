package com.nostratech.movieticketing.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nostratech.movieticketing.domain.City;

public interface CityRepository extends JpaRepository<City, Long>{
	
	Optional<City> findCityIdById (Long id);

}
