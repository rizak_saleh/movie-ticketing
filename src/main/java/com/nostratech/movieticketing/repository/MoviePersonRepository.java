package com.nostratech.movieticketing.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nostratech.movieticketing.domain.MoviePerson;

public interface MoviePersonRepository extends JpaRepository<MoviePerson, Long>{

	List<MoviePerson> findByIdIn(List<Long> idList);
	
	List<MoviePerson> findByMovieId(Long idList);
	
	Optional<MoviePerson> findById(Long id);
}
 