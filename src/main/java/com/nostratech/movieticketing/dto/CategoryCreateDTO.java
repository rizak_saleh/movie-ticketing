package com.nostratech.movieticketing.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class CategoryCreateDTO {

	@NotBlank
	private String categoryName;

}
