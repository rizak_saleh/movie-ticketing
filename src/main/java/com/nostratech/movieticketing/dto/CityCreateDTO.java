package com.nostratech.movieticketing.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class CityCreateDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2077221207931428468L;
	
	@NotBlank
	private String cityName;

}
