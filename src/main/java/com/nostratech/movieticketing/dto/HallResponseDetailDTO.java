package com.nostratech.movieticketing.dto;

import java.io.Serializable;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class HallResponseDetailDTO implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 8776532012863466327L;

	private String hallId;

	private String hallName;

	private String theaterName;
	
	private String cityName;

}
