package com.nostratech.movieticketing.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.nostratech.movieticketing.domain.MoviePerson;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class MovieDetailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7889938648939242355L;

	private Long movieId;

	private String movieTitle;

	private String movieDescription;
	
//	private List<Long> categoryIdList;
//	
//	private List<MoviePerson> moviePersonIdList;
	
}
