package com.nostratech.movieticketing.dto;

import java.io.Serializable;

public class CategoryRequestDTO implements Serializable {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
