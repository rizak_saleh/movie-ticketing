package com.nostratech.movieticketing.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class PositionDetailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7889938648939242355L;

	private Long positionId;

	private String positionName;

}
