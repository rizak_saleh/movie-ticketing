package com.nostratech.movieticketing.dto;

import java.io.Serializable;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class MovieResponsePageDTO implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7985167775785479214L;

	private String movieId;
	
	private String MovieTitle;

}
