package com.nostratech.movieticketing.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


//This is a paging payload
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ResultPageResponseDTO<T> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3881909537246350985L;

	private List<T> result;
	
	private Integer pages;
	
	private Long elements; //menampilkan jumlah records pada suatau query

	public List<T> getResult() {
		return result;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}

	public Integer getPages() {
		return pages;
	}

	public void setPages(Integer pages) {
		this.pages = pages;
	}

	public Long getElements() {
		return elements;
	}

	public void setElements(Long elements) {
		this.elements = elements;
	}

}
