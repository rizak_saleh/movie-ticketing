package com.nostratech.movieticketing.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class CategoryDetailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7889938648939242355L;

	private Long categoryId;

	private String categoryName;

}
