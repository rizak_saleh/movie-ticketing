package com.nostratech.movieticketing.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PersonDetailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7889938648939242355L;

	private Long personId;

	private String personFirstName;

	private String personLastName;
	
}
