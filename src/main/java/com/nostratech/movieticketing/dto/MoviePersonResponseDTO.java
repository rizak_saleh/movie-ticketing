package com.nostratech.movieticketing.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.nostratech.movieticketing.domain.Movie;
import com.nostratech.movieticketing.domain.Person;
import com.nostratech.movieticketing.domain.Position;

import lombok.Data;


@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class MoviePersonResponseDTO implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -1885594989213918194L;

	
	private MovieDTO movie;
	
	private String firstName;
	
	private String lastName;
	
	private String position;

	public MovieDTO getMovie() {
		return movie;
	}

	public void setMovie(MovieDTO movie) {
		this.movie = movie;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	
	
}
