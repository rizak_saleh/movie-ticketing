package com.nostratech.movieticketing.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PersonCreateDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6608351016104235530L;

	@NotBlank
	private String personFirstName;

	@NotBlank
	private String personLastName;
	
}
