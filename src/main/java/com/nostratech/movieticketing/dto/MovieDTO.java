package com.nostratech.movieticketing.dto;

import java.io.Serializable;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;


@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class MovieDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7704356573423973615L;

	private Long id;
	
	private String title;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	

}
