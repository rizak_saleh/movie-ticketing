package com.nostratech.movieticketing.dto;

import java.io.Serializable;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class HallResponseDTO implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = -1840775169228293223L;
	
	private String hallId;
	
	private String hallName;
	
	private String theaterName;

}
