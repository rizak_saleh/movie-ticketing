package com.nostratech.movieticketing.dto;

import java.io.Serializable;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PersonResponseDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7400850320962300724L;

	private Long id;
	
	private String firstName;
	
	private String lastName;

}
