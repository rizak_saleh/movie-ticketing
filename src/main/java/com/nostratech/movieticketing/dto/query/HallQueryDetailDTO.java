package com.nostratech.movieticketing.dto.query;

import java.io.Serializable;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class HallQueryDetailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5313839033305060311L;

	private Long hallId;

	private String hallName;

	private String theaterName;
	
	private String cityName;

}
