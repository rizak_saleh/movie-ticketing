package com.nostratech.movieticketing.dto.query;

import java.io.Serializable;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class HallQueryDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3356008730999250L;

	private Long hallId;
	
	private String hallName;
	
	private String theaterName;

}
