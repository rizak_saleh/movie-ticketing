package com.nostratech.movieticketing.dto.query;

import java.io.Serializable;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class TheaterQueryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8984904018924144067L;

	private Long theaterId;

	private String theaterName;
	
	private String cityName;

}
