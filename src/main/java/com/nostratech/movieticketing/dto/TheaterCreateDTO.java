package com.nostratech.movieticketing.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class TheaterCreateDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5836402155642605385L;

	@NotBlank
	private String theaterName;
	
	@NotBlank
	private Long cityId;

}
