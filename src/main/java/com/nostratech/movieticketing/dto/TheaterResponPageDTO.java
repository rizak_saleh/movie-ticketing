package com.nostratech.movieticketing.dto;

import java.io.Serializable;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class TheaterResponPageDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2938288321850610239L;

	private String theaterId;
	
	private String theaterName;

}
