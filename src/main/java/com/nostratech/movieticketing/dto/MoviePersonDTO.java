package com.nostratech.movieticketing.dto;

import java.io.Serializable;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.nostratech.movieticketing.domain.Person;
import com.nostratech.movieticketing.domain.Position;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class MoviePersonDTO implements Serializable{

	private static final long serialVersionUID = 1754407771467239635L;

	private Person person;
	
	private Position position;
	
	
}
