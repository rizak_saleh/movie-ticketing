package com.nostratech.movieticketing.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class MovieRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5447495049709335535L;

	private String title;
	
	private String description;

	private List<Long> categoryIdList;
	
	private List<MoviePersonDTO> moviePersonIdList;
	
}
