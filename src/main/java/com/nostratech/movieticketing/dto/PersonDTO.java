package com.nostratech.movieticketing.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;


@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PersonDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7889938648939242355L;

	private String firstName;

	private String lastName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}
