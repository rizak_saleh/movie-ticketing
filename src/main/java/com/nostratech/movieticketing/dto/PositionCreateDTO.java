package com.nostratech.movieticketing.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class PositionCreateDTO  implements Serializable{

	
	
	@NotBlank
	private String positionName;

}
