package com.nostratech.movieticketing.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class MovieDetailResponseDTO implements Serializable {

	private Long id;
	
	private String title;
	
	private String description;
	
	private List<CategoryDTO> categories;
	
	private List<MoviePersonDTO> moviePersons;
	
	
}
