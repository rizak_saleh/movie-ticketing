package com.nostratech.movieticketing.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Entity
@Table(name = "cinema")
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Cinema implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 8650863541374555017L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "start_date", nullable = false)
	private Date startDate;
	
	@Column(name = "finish_date", nullable = false)
	private Date finishDate;
	
	@ManyToOne
	@JoinColumn(name = "movie_id", nullable = false)
	private Movie movie;
	
	@ManyToOne
	@JoinColumn(name = "theater_id", nullable = false)
	private Theater theater;
	

}
