package com.nostratech.movieticketing.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;
@Entity
@Table(name = "category")
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Category implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4709181905976901797L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	
}
