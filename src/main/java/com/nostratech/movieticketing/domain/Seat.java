package com.nostratech.movieticketing.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Entity
@Table(name = "seat")
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Seat implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = -2423525029322496025L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "num", nullable = false)
	private Long num;
	
	@Column(name = "pos_x", nullable = false)
	private Long pos_x;
	
	@Column(name = "pos_y", nullable = false)
	private Long pos_y;
	
	@ManyToOne
	@JoinColumn(name = "hall_id", nullable = false)
	private Hall hall;

}
