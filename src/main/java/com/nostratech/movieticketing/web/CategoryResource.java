package com.nostratech.movieticketing.web;

import java.net.URI;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nostratech.movieticketing.dto.CategoryCreateDTO;
import com.nostratech.movieticketing.dto.CategoryDetailDTO;
import com.nostratech.movieticketing.dto.CategoryListResponseDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;
import com.nostratech.movieticketing.service.CategoryService;


import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class CategoryResource {

	private final CategoryService categoryService;

	@GetMapping("/v1/category")
	public ResponseEntity<List<CategoryDetailDTO>> findCategoryList() {
		List<CategoryDetailDTO> categories = categoryService.findCategoryListDetail();
		return ResponseEntity.ok().body(categories);
	}
	
	
	@PostMapping("/v1/category")
	public ResponseEntity<Void> createNewCategory(@RequestBody CategoryCreateDTO dto){
		categoryService.createNewCategory(dto);
		return ResponseEntity.created(URI.create("/category")).build();
	}
	
	@GetMapping("v2/category")
	public ResponseEntity<ResultPageResponseDTO<CategoryListResponseDTO>> findCategoryListPagination(
			@RequestParam(name = "pages", required = true, defaultValue = "0") Integer pages,
			@RequestParam(name = "limit", required = true, defaultValue = "10") Integer limit,
			@RequestParam(name = "sortBy", required = false, defaultValue = "name") String sortBy,
			@RequestParam(name = "direction", required = false, defaultValue = "asc") String direction)
//			@RequestParam(name = "categoryName", required = false) String categoryName)
	{
		return ResponseEntity.ok().body(categoryService.findCategoryList(pages, limit, sortBy, direction));
	}
}
