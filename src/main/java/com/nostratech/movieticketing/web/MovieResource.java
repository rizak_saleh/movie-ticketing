package com.nostratech.movieticketing.web;

import java.net.URI;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nostratech.movieticketing.dto.CategoryResponseDTO;
import com.nostratech.movieticketing.dto.MovieCreateDTO;
import com.nostratech.movieticketing.dto.MovieDetailDTO;
import com.nostratech.movieticketing.dto.MovieDetailResponseDTO;
import com.nostratech.movieticketing.dto.MovieListResponseDTO;
import com.nostratech.movieticketing.dto.MoviePersonResponseDTO;
import com.nostratech.movieticketing.dto.MovieUpdateRequestDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;
import com.nostratech.movieticketing.service.CategoryService;
import com.nostratech.movieticketing.service.MoviePersonService;
import com.nostratech.movieticketing.service.MovieService;


import lombok.AllArgsConstructor;
import lombok.Delegate;

@AllArgsConstructor
@RestController
public class MovieResource {

	private final MovieService movieService;
	
	private final CategoryService categoryService;
	
	private final MoviePersonService moviePersonService;

	@GetMapping("/v1/movie")
	public ResponseEntity<List<MovieDetailDTO>> findMovieList() {
		List<MovieDetailDTO> movies = movieService.findMovieListDetail();
		return ResponseEntity.ok().body(movies);
	}
	
	@GetMapping("/v1/movie/{id}")
	public ResponseEntity<MovieDetailResponseDTO> findMovie(@PathVariable Long id) {
	
		MovieDetailResponseDTO dto = movieService.findMovieDetail(id);
		return ResponseEntity.ok().body(dto);
	}
	
	@GetMapping("/v1/category/{id}")
	public ResponseEntity<CategoryResponseDTO> findCategory(@PathVariable Long id) {
	
		CategoryResponseDTO dto = categoryService.findCategoryDetail(id);
		return ResponseEntity.ok().body(dto);
	}
	
	@GetMapping("/v1/movie-person/{id}")
	public ResponseEntity<MoviePersonResponseDTO> findMoviePerson(@PathVariable Long id){
		MoviePersonResponseDTO dto = moviePersonService.findMoviePersonDetail(id);
		return ResponseEntity.ok().body(dto);
	}
	
	@PostMapping("/v1/movie")
	public ResponseEntity<Void> createNewMovie(@RequestBody MovieCreateDTO dto){
		movieService.createNewMovie(dto);
		return ResponseEntity.created(URI.create("/v1/movie")).build();
	}
	
	@PutMapping("/v1/movie/{movieId}")
	public ResponseEntity<Void> updateMovie(@PathVariable Long movieId, @RequestBody MovieUpdateRequestDTO dto){
		movieService.updateMovie(movieId, dto);
		return ResponseEntity.ok().build();
	}
	
	@DeleteMapping("/v1/movie/{movieId}")
	public ResponseEntity<Void> deleteMovie(@PathVariable Long movieId){
		movieService.deleteMovie(movieId);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/v2/movie")
	public ResponseEntity<ResultPageResponseDTO<MovieListResponseDTO>> findMovieListPagination(
			@RequestParam(name = "pages", required = true, defaultValue = "0") Integer pages,
			@RequestParam(name = "limit", required = true, defaultValue = "10") Integer limit,
			@RequestParam(name = "sortBy", required = true, defaultValue = "title") String sortBy,
			@RequestParam(name = "direction", required = true, defaultValue = "asc") String direction)
			{
		return ResponseEntity.ok().body(movieService.findMovieList(pages, limit, sortBy, direction));
	}
	
}
