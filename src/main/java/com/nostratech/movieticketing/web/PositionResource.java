package com.nostratech.movieticketing.web;

import java.net.URI;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nostratech.movieticketing.dto.PositionCreateDTO;
import com.nostratech.movieticketing.dto.PositionDetailDTO;
import com.nostratech.movieticketing.dto.PositionListResponseDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;
import com.nostratech.movieticketing.service.PositionService;


import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class PositionResource {

	private final PositionService positionService;

	@GetMapping("/v1/position")
	public ResponseEntity<List<PositionDetailDTO>> findPositionList() {
		List<PositionDetailDTO> categories = positionService.findPositionListDetail();
		return ResponseEntity.ok().body(categories);
	}
	
	
	@PostMapping("/v1/position")
	public ResponseEntity<Void> createNewPosition(@RequestBody PositionCreateDTO dto){
		positionService.createNewPosition(dto);
		return ResponseEntity.created(URI.create("/position")).build();
	}
	@GetMapping("/v2/position")
	public ResponseEntity<ResultPageResponseDTO<PositionListResponseDTO>> findPositionListPagination(
			@RequestParam(name = "pages", required = true, defaultValue = "0") Integer pages,
			@RequestParam(name = "limit", required = true, defaultValue = "10") Integer limit,
			@RequestParam(name = "sortBy", required = false, defaultValue = "name") String sortBy,
			@RequestParam(name = "direction", required = false, defaultValue = "asc") String direction,
			@RequestParam(name = "name", required = true) String name)
			{
		return ResponseEntity.ok().body(positionService.findPositionList(pages, limit, sortBy, direction, name));
	}
}
