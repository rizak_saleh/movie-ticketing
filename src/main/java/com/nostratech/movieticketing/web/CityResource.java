package com.nostratech.movieticketing.web;

import java.net.URI;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.nostratech.movieticketing.dto.CityCreateDTO;
import com.nostratech.movieticketing.service.CityService;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class CityResource {
	
	private final CityService cityService;
	
	@PostMapping("/v1/city")
	public ResponseEntity<Void> createNewCity(@RequestBody CityCreateDTO dto){
		cityService.createNewCity(dto);
		return ResponseEntity.created(URI.create("/city")).build();
	}

}
