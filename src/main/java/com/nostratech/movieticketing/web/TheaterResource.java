package com.nostratech.movieticketing.web;

import java.net.URI;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nostratech.movieticketing.dto.ResultPageResponseDTO;
import com.nostratech.movieticketing.dto.TheaterCreateDTO;
import com.nostratech.movieticketing.dto.TheaterResponPageDTO;
import com.nostratech.movieticketing.service.TheaterService;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class TheaterResource {
	
	private final TheaterService theaterService;
	
	@PostMapping("/v1/theater")
	public ResponseEntity<Void> createNewTheater (@RequestBody TheaterCreateDTO dto){
		theaterService.createNewTheater(dto);
		return ResponseEntity.created(URI.create("/v1/theater")).build();
	}
	
	@GetMapping("/v1/theater")
	public ResponseEntity<ResultPageResponseDTO<TheaterResponPageDTO>> findTheaterPagination(
			@RequestParam(name = "pages", required = true, defaultValue = "0") Integer pages,
			@RequestParam(name = "limit", required = true, defaultValue = "10") Integer limit,
			@RequestParam(name = "sortBy", required = false, defaultValue = "name") String sortBy,
			@RequestParam(name = "direction", required = false, defaultValue = "asc") String direction,
			@RequestParam(name = "cityName", required = false, defaultValue = "") String cityName) {
		return ResponseEntity.ok().body(theaterService.findPersonList(pages, limit, sortBy, direction, cityName));
	}

}
