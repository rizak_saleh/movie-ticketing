package com.nostratech.movieticketing.web;

import java.net.URI;
import java.util.List;

import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nostratech.movieticketing.dto.PersonCreateDTO;
import com.nostratech.movieticketing.dto.PersonDetailDTO;
import com.nostratech.movieticketing.dto.PersonListResponDTO;
import com.nostratech.movieticketing.dto.ResultPageResponseDTO;
import com.nostratech.movieticketing.service.PersonService;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class PersonResource {

	private final PersonService personService;

	@GetMapping("/v1/person")
	public ResponseEntity<List<PersonDetailDTO>> findPersonList() {
		List<PersonDetailDTO> persons = personService.findPersonListDetail();
		return ResponseEntity.ok().body(persons);
	}

	@PostMapping("/v1/person")
	public ResponseEntity<Void> createNewPerson(@RequestBody PersonCreateDTO dto) {
		personService.createNewPerson(dto);
		return ResponseEntity.created(URI.create("/person")).build();
	}

	@GetMapping("/v2/person")
	public ResponseEntity<ResultPageResponseDTO<PersonListResponDTO>> findPersonListPagination(
			@RequestParam(name = "pages", required = true, defaultValue = "0") Integer pages,
			@RequestParam(name = "limit", required = true, defaultValue = "10") Integer limit,
			@RequestParam(name = "sortBy", required = false, defaultValue = "id") String sortBy,
			@RequestParam(name = "direction", required = false, defaultValue = "asc") String direction,
			@RequestParam(name = "firstName", required = false, defaultValue = "") String firstName) {
		return ResponseEntity.ok().body(personService.findPersonList(pages, limit, sortBy, direction, firstName));
	}

}
