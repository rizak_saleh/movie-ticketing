--CREATE MOVIE
---------------------------------------------------------------------
CREATE TABLE Movies
(ID integer PRIMARY KEY, Title varchar(255) NOT NULL, Duration integer, Release_date date);

CREATE TABLE Categories
(ID integer PRIMARY KEY, Name varchar(255));

CREATE TABLE Trailer
(ID integer PRIMARY KEY, Source varchar(255), URL varchar(255));

CREATE TABLE Person
(ID integer PRIMARY KEY, First_Name varchar(255), Last_Name varchar(255), Birthdate date);

CREATE TABLE Position
(ID integer PRIMARY KEY, Name varchar(255));

CREATE TABLE Movie_Category(
movie_id int NOT NULL,
category_id int NOT NULL,
CONSTRAINT fk_movie_category_movie_id
FOREIGN KEY (movie_id) REFERENCES movies(id),
CONSTRAINT fk_movie_category_category_id
FOREIGN KEY (category_id) REFERENCES categories(id)
);

CREATE TABLE Movie_Trailer(
movie_id int NOT NULL,
Trailer_id int NOT NULL,
CONSTRAINT fk_movie_trailer_movie_id
FOREIGN KEY (movie_id) REFERENCES movies(id),
CONSTRAINT fk_movie_trailer_trailer_id
FOREIGN KEY (trailer_id) REFERENCES trailer(id)
);

CREATE TABLE movie_person(
movie_id int NOT NULL,
person_id int NOT NULL,
position_id int NOT NULL,
CONSTRAINT fk_movie_person_movie_id
FOREIGN KEY (movie_id) REFERENCES movies(id),
CONSTRAINT fk_movie_person_person_id
FOREIGN KEY (person_id) REFERENCES person(id),
CONSTRAINT fk_movie_person_position_id
FOREIGN KEY (position_id) REFERENCES position(id)
);





--Movie_Category
---------------------------------------------------------------------
--Update movie tambah category
INSERT INTO movie_category VALUES(movie_id, newCategory_id);

--Update movie update category
UPDATE movie_category
SET category_id = 5
WHERE movie_id = 3;

--update movie delete category
DELETE FROM movie_category
WHERE movie_id = 4; -- atau WHERE movie_id = 4 AND category_id = 6;

--View movie_category
SELECT * FROM movie_category;


--Movie_Trailer
---------------------------------------------------------------------
--Update movie tambah trailer
INSERT INTO movie_trailer VALUES(movie_id, newtrailer_id);

--Update movie update trailer
UPDATE movie_trailer
SET trailer_id = 9
WHERE movie_id = 3;

--update movie delete trailer
DELETE FROM movie_trailer
WHERE movie_id = 4; -- atau WHERE movie_id = 4 AND trailer_id = 6;

--View movie_category
SELECT * FROM movie_trailer;


####--Movie_Person
---------------------------------------------------------------------
--Update movie tambah person
INSERT INTO movie_person VALUES(movie_id, newPerson_id, position_id);

--Update movie tambah position
INSERT INTO movie_person VALUES(movie_id, Person_id, newPosition_id);

--Update movie update person
UPDATE movie_person
SET person_id = 8
WHERE movie_id = 3;

--Update movie update position
UPDATE movie_position
SET position_id = 2
WHERE person_id = 3 and position_id = 4;

--update movie delete category
DELETE FROM movie_category
WHERE movie_id = 4; -- atau WHERE movie_id = 4 AND category_id = 6;

--View movie_person
SELECT * FROM movie_person;



--Category
--------------------------------------------------------------------
--Insert Category
INSERT INTO categories (id, name)
VALUES(13, 'horror');

--Update Category
UPDATE categories 
SET name = 'romance'
WHERE id = 13;

--Delete Category
DELETE FROM categories
WHERE id =13;

--View category
SELECT * FROM category;


--TRAILER
--------------------------------------------------------------------
--Insert trailer
INSERT INTO trailer (id, source, url)
VALUES(20, 'video.com', http;//video.com/blablabla);

--Update trailer
UPDATE Trailer 
SET source = 'youtube'
WHERE id = 2;

--Delete trailer
DELETE FROM trailer
WHERE id =20;

--View Trailer
SELECT * FROM trailer;


-- POSITION
--------------------------------------------------------------------
--Insert position
INSERT INTO position (id, name)
VALUES(5, 'cameraman');

--Update position
UPDATE position
SET name = 'production assistant'
WHERE id = 5;

--Delete position
DELETE FROM position
WHERE name= 'production assistant';

--View position
SELECT * FROM position;


-- PERSON
--------------------------------------------------------------------
--Insert person
INSERT INTO person (id, first_name, last_name)
VALUES(8, 'Leonardo', 'Dicaprio');

--Update person
UPDATE person
SET birthdate = '1974-11-11'
WHERE id = 8;

--Delete person
DELETE FROM person
WHERE id= 8;

--View person
SELECT * FROM person;




