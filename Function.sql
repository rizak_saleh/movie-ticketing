----------------------------------------------INSERT FUNCTION
-- Insert Category
insert into categories values(7, 'Romance');

-- Insert Person
insert into person values(8, 'Leonardo', 'Dicaprio', '1974-11-11');

-- insert Position(status)
insert into "position"  values(5, 'cameraman');


-- insert MOVIE
insert into movies values(9, 'Titanic', '195', '1997-11-01');
insert into trailer values(15, 'video.com', 'http;//video.com/blablabla');
insert into movie_trailer values(9, 15);
insert into movie_category values(9, 7);
insert into movie_person values(9, 8, 4);


------------------------------------------- UPDATE FUNCTION
--Update Category
UPDATE categories 
SET "name"  = 'horror' WHERE id = 7;

--Update trailer 
UPDATE Trailer SET "source" = 'youtube' WHERE id = 16;

--Update position
UPDATE "position"  SET "name"  = 'Producer' WHERE id = 1;

--Update person
UPDATE person SET birthdate = '1974-11-11' WHERE id = 8;

-- Update Movie relase date
update movies set release_date = '1997-09-28' WHERE id = 8;

--update movie category with add new category
insert into movie_category values (9, 1); 

--update movie category with add new trailer
insert into trailer values(16, 'viva.com', 'http;//viva.com/contoh');
insert into movie_trailer values (9, 16);


------------------------------------------- DELETE FUNCTION
--delete movie
delete from movie_category where movie_id = 9 and category_id =7;
delete from movie_person  where movie_id = 9 and person_id =8;
delete from movie_trailer where movie_id = 9 and trailer_id = 15;

delete from movies where id = 9

--Delete trailer
DELETE FROM trailer WHERE id =15;

--Delete person
DELETE FROM person WHERE id= 8;

--Delete Category
DELETE FROM categories WHERE id =7;

--Delete position(status)
DELETE FROM "position"  WHERE "name"  = 'cameraman';


--VIEW TABLE 
drop view if exists view_movie;
create or replace view view_movie as
select m.id, 
m.title, 
c."name" as cat_name, 
p.first_name, 
p.last_name, 
po."name" as pos_name
from movies m
INNER JOIN movie_category mc ON mc.movie_id = m.id
INNER JOIN categories c ON c.id = mc.category_id
INNER JOIN movie_person mp ON mp.movie_id = m.id
INNER JOIN person p ON p.id = mp.person_id
INNER JOIN "position" po ON po.id = mp.position_id;


--VIEW Category Movie 
drop view if exists view_movie_category ;
create or replace view view_movie_category as
select c.name, m.title from categories c 
inner join movie_category mc on mc.category_id = c.id
inner join movies m on m.id = mc.movie_id
where c.name ='Superhero';






