--Create Main Tabel
CREATE TABLE Movies
(ID integer PRIMARY KEY, Title varchar(255) NOT NULL, Duration integer, Release_date date);

CREATE TABLE Categories
(ID integer PRIMARY KEY, Name varchar(255));

CREATE TABLE Trailer
(ID integer PRIMARY KEY, Source varchar(255), URL varchar(255));

CREATE TABLE Person
(ID integer PRIMARY KEY, First_Name varchar(255), Last_Name varchar(255), Birthdate date);

CREATE TABLE Position
(ID integer PRIMARY KEY, Name varchar(255));



--Create Join Table
----------------------------------------------------------------------------
CREATE TABLE Movie_Category(
movie_id int NOT NULL,
category_id int NOT NULL,
CONSTRAINT fk_movie_category_movie_id
FOREIGN KEY (movie_id) REFERENCES movies(id),
CONSTRAINT fk_movie_category_category_id
FOREIGN KEY (category_id) REFERENCES categories(id)
);

CREATE TABLE Movie_Trailer(
movie_id int NOT NULL,
Trailer_id int NOT NULL,
CONSTRAINT fk_movie_trailer_movie_id
FOREIGN KEY (movie_id) REFERENCES movies(id),
CONSTRAINT fk_movie_trailer_trailer_id
FOREIGN KEY (trailer_id) REFERENCES trailer(id)
);

CREATE TABLE movie_person(
movie_id int NOT NULL,
person_id int NOT NULL,
position_id int NOT NULL,
CONSTRAINT fk_movie_person_movie_id
FOREIGN KEY (movie_id) REFERENCES movies(id),
CONSTRAINT fk_movie_person_person_id
FOREIGN KEY (person_id) REFERENCES person(id),
CONSTRAINT fk_movie_person_position_id
FOREIGN KEY (position_id) REFERENCES position(id)
);

--INSERT data Movies
--------------------------------------------------------------------------
INSERT INTO movies VALUES(1, 'No Way Home',148, '2021-12-15');
INSERT INTO movies VALUES(2, 'Dune',155, '2021-10-13');
INSERT INTO movies VALUES(3, 'Uncharted',116, '2022-02-16');
INSERT INTO movies VALUES(4, 'The Dark Knight',152, '2008-07-14');
INSERT INTO movies VALUES(5, 'Eternals',156, '2021-10-18');
INSERT INTO movies VALUES(6, 'Maleficent',97, '2014-05-28');
INSERT INTO movies VALUES(7, 'The Great Dictator',125, '1940-10-15');
INSERT INTO movies VALUES(8, 'Taste of Cherry',95, '1997-09-28');

--Insert data categories
--------------------------------------------------------------------------
INSERT INTO categories VALUES(1, 'Sci-Fi');
INSERT INTO categories VALUES(2, 'Superhero');
INSERT INTO categories VALUES(3, 'Advanture');
INSERT INTO categories VALUES(4, 'Fantasy');
INSERT INTO categories VALUES(5, 'Comedy');
INSERT INTO categories VALUES(6, 'Drama');

--Insert data person
--------------------------------------------------------------------------
INSERT INTO person VALUES(1, 'Tom', 'Holland', '1996-06-01');
INSERT INTO person VALUES(2, 'Christian', 'Bale', '1974-01-30');
INSERT INTO person VALUES(3, 'Richard', 'Madden', '1986-06-18');
INSERT INTO person VALUES(4, 'Zendaya', '', '1996-09-01');
INSERT INTO person VALUES(5, 'Angelina', 'Jolie', '1975-06-04');
INSERT INTO person VALUES(6, 'Charlie', 'Chaplin', '1889-04-16');
INSERT INTO person VALUES(7, 'Abbas', 'Kiarostami', '1940-06-22');


--Insert data Position
--------------------------------------------------------------------------
INSERT INTO Position VALUES (1, 'Producer');
INSERT INTO Position VALUES (2, 'Director');
INSERT INTO Position VALUES (3, 'Screenwriter');
INSERT INTO Position VALUES (4, 'Actor');


--Insert Data Trailer
--------------------------------------------------------------------------
INSERT INTO Trailer VALUES (1, 'Youtube', 'https://youtu.be/JfVOs4VSpmA');
INSERT INTO Trailer VALUES (2, 'Dunemovie.net', 'https://www.dunemovie.net/videos/');
INSERT INTO Trailer VALUES (3, 'Youtube', 'https://youtu.be/eHp3MbsCbMg');
INSERT INTO Trailer VALUES (4, 'Youtube', 'https://youtu.be/4wCH1K-ckZw');
INSERT INTO Trailer VALUES (5, 'Youtube', 'https://youtu.be/EXeTwQWrcwY');
INSERT INTO Trailer VALUES (6, 'Youtube', 'https://youtu.be/0WVDKZJkGlY');
INSERT INTO Trailer VALUES (7, 'Youtube', 'https://youtu.be/x_me3xsvDgk');
INSERT INTO Trailer VALUES (8, 'Youtube', 'https://youtu.be/704EXbJ-b5k');
INSERT INTO Trailer VALUES (9, 'Youtube', 'https://youtu.be/_pgmFAOgm5E');
INSERT INTO Trailer VALUES (10, 'Youtube', 'https://youtu.be/w-XO4XiRop0');
INSERT INTO Trailer VALUES (11, 'Youtube', 'https://youtu.be/i1C6qZVeFtA');
INSERT INTO Trailer VALUES (12, 'Youtube', 'https://youtu.be/k8bVG8XC-4I');
INSERT INTO Trailer VALUES (13, 'Youtube', 'https://youtu.be/H2NhAmDv1dw');
INSERT INTO Trailer VALUES (14, 'Youtube', 'https://youtu.be/550uwQQeNeo');


--Insert data Movie_Category
----------------------------------------------------------------------------
INSERT INTO movie_category VALUES(1, 1);
INSERT INTO movie_category VALUES(1, 2);
INSERT INTO movie_category VALUES(1, 3);
INSERT INTO movie_category VALUES(1, 4);
INSERT INTO movie_category VALUES(1, 5);
INSERT INTO movie_category VALUES(2, 1);
INSERT INTO movie_category VALUES(2, 3);
INSERT INTO movie_category VALUES(2, 4);
INSERT INTO movie_category VALUES(2, 6);
INSERT INTO movie_category VALUES(3, 3);
INSERT INTO movie_category VALUES(4, 2);
INSERT INTO movie_category VALUES(4, 3);
INSERT INTO movie_category VALUES(4, 6);
INSERT INTO movie_category VALUES(5, 1);
INSERT INTO movie_category VALUES(5, 2);
INSERT INTO movie_category VALUES(5, 3);
INSERT INTO movie_category VALUES(5, 4);
INSERT INTO movie_category VALUES(5, 6);
INSERT INTO movie_category VALUES(6, 3);
INSERT INTO movie_category VALUES(6, 4);
INSERT INTO movie_category VALUES(7, 5);
INSERT INTO movie_category VALUES(7, 6);
INSERT INTO movie_category VALUES(8, 6);

--Insert data Movie_Trailer
----------------------------------------------------------------------------

INSERT INTO movie_trailer VALUES(1, 1);
INSERT INTO movie_trailer VALUES(2, 2);
INSERT INTO movie_trailer VALUES(3, 3);
INSERT INTO movie_trailer VALUES(3, 4);
INSERT INTO movie_trailer VALUES(4, 5);
INSERT INTO movie_trailer VALUES(5, 6);
INSERT INTO movie_trailer VALUES(5, 7);
INSERT INTO movie_trailer VALUES(6, 8);
INSERT INTO movie_trailer VALUES(6, 9);
INSERT INTO movie_trailer VALUES(6, 10);
INSERT INTO movie_trailer VALUES(7, 11);
INSERT INTO movie_trailer VALUES(7, 12);
INSERT INTO movie_trailer VALUES(8, 13);
INSERT INTO movie_trailer VALUES(8, 14);


--Insert data Movie_Person
----------------------------------------------------------------------------
INSERT INTO movie_person VALUES(1, 1, 4);
INSERT INTO movie_person VALUES(1, 4, 4);
INSERT INTO movie_person VALUES(2, 4, 4);
INSERT INTO movie_person VALUES(3, 1, 4);
INSERT INTO movie_person VALUES(4, 2, 4);
INSERT INTO movie_person VALUES(5, 5, 4);
INSERT INTO movie_person VALUES(6, 5, 4);
INSERT INTO movie_person VALUES(7, 6, 1);
INSERT INTO movie_person VALUES(7, 6, 2);
INSERT INTO movie_person VALUES(7, 6, 3);
INSERT INTO movie_person VALUES(7, 6, 4);
INSERT INTO movie_person VALUES(8, 7, 1);
INSERT INTO movie_person VALUES(8, 7, 2);
INSERT INTO movie_person VALUES(8, 7, 3);


-- Showing Movie Category
SELECT m.id, m.title, c.name FROM Movies m
INNER JOIN movie_category mc ON mc.movie_id = m.id
INNER JOIN categories c ON c.id = mc.category_id;

-- Showing Movie Trailer
SELECT m.id, m.title, t.source, t.url FROM Movies m
INNER JOIN movie_trailer mt ON mt.movie_id = m.id
INNER JOIN trailer t ON t.id = mt.trailer_id;

-- Showing Movie Person
SELECT m.id, m.title, p.first_name, p.last_name, po.name FROM Movies m
INNER JOIN movie_person mp ON mp.movie_id = m.id
INNER JOIN person p ON p.id = mp.person_id
INNER JOIN position po ON po.id = mp.position_id;



